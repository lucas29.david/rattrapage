#include "../include/draw.h"

void draw(bool toit){
    glPushMatrix();
    glDepthMask( GL_TRUE );
        // Terrain total (+jardin)
        glBegin(GL_TRIANGLES);
            glColor3f(0.125f, 0.443f, 0.145f);
            glVertex3f(-10, -0.1, 10);
            glVertex3f(-10, -0.1, -10);
            glVertex3f(10, -0.1, -10);
            glVertex3f(-10, -0.1, 10);
            glVertex3f(10, -0.1, 10);
            glVertex3f(10, -0.1, -10);
        glEnd();

        glPushMatrix();
            glTranslatef(-6.5, 0.0, -5.5);
            // Sol maison
            glBegin(GL_POLYGON);
                glColor3f(0.2f, 0.2f, 0.2f);
                glVertex3f(0, 0, 0);
                glVertex3f(0, 0, 11);
                glVertex3f(3, 0, 11);
                glVertex3f(3, 0, 8);
                glVertex3f(13, 0, 8);
                glVertex3f(13, 0, 0);
            glEnd();

            // Murs maison (extérieurs)
            glBegin(GL_QUADS);
                glColor3f(0.8f, 0.8f, 0.8f);
                glVertex3f(0, 0, 0);
                glVertex3f(0, 0, 8);
                glVertex3f(0, 3, 8);
                glVertex3f(0, 3, 0);

                glVertex3f(0, 0, 8);
                glVertex3f(0, 0, 11);
                glVertex3f(0, 2.25, 11);
                glVertex3f(0, 2.25, 8);

                glVertex3f(0, 0, 11);
                glVertex3f(0.5, 0, 11);
                glVertex3f(0.5, 2.25, 11);
                glVertex3f(0, 2.25, 11);

                glVertex3f(0.5, 2, 11);
                glVertex3f(2.5, 2, 11);
                glVertex3f(2.5, 2.25, 11);
                glVertex3f(0.5, 2.25, 11);

                glVertex3f(2.5, 0, 11);
                glVertex3f(3, 0, 11);
                glVertex3f(3, 2.25, 11);
                glVertex3f(2.5, 2.25, 11);

                glVertex3f(3, 0, 11);
                glVertex3f(3, 0, 8);
                glVertex3f(3, 2.25, 8);
                glVertex3f(3, 2.25, 11);

                glVertex3f(3, 0, 8);
                glVertex3f(5.75, 0, 8);
                glVertex3f(5.75, 3, 8);
                glVertex3f(3, 3, 8);

                glVertex3f(5.75, 2, 8);
                glVertex3f(7.25, 2, 8);
                glVertex3f(7.25, 3, 8);
                glVertex3f(5.75, 3, 8);

                glVertex3f(7.25, 0, 8);
                glVertex3f(8.25, 0, 8);
                glVertex3f(8.25, 3, 8);
                glVertex3f(7.25, 3, 8);

                glVertex3f(8.25, 2, 8);
                glVertex3f(9.25, 2, 8);
                glVertex3f(9.25, 3, 8);
                glVertex3f(8.25, 3, 8);

                glVertex3f(9.25, 0, 8);
                glVertex3f(10.5, 0, 8);
                glVertex3f(10.5, 3, 8);
                glVertex3f(9.25, 3, 8);

                glVertex3f(10.5, 2, 8);
                glVertex3f(12, 2, 8);
                glVertex3f(12, 3, 8);
                glVertex3f(10.5, 3, 8);

                glVertex3f(12, 0, 8);
                glVertex3f(13, 0, 8);
                glVertex3f(13, 3, 8);
                glVertex3f(12, 3, 8);

                glVertex3f(13, 0, 8);
                glVertex3f(13, 0, 2.5);
                glVertex3f(13, 3, 2.5);
                glVertex3f(13, 3, 8);

                glVertex3f(13, 2, 2.5);
                glVertex3f(13, 2, 1);
                glVertex3f(13, 3, 1);
                glVertex3f(13, 3, 2.5);

                glVertex3f(13, 0, 1);
                glVertex3f(13, 0, 0);
                glVertex3f(13, 3, 0);
                glVertex3f(13, 3, 1);

                glVertex3f(13, 0, 0);
                glVertex3f(8.25, 0, 0);
                glVertex3f(8.25, 3, 0);
                glVertex3f(13, 3, 0);

                glVertex3f(8.25, 0, 0);
                glVertex3f(6.75, 0, 0);
                glVertex3f(6.75, 1, 0);
                glVertex3f(8.25, 1, 0);

                glVertex3f(8.25, 2.5, 0);
                glVertex3f(6.75, 2.5, 0);
                glVertex3f(6.75, 3, 0);
                glVertex3f(8.25, 3, 0);

                glVertex3f(6.75, 0, 0);
                glVertex3f(5.25, 0, 0);
                glVertex3f(5.25, 3, 0);
                glVertex3f(6.75, 3, 0);

                glVertex3f(5.25, 0, 0);
                glVertex3f(3.75, 0, 0);
                glVertex3f(3.75, 1, 0);
                glVertex3f(5.25, 1, 0);

                glVertex3f(5.25, 2.5, 0);
                glVertex3f(3.75, 2.5, 0);
                glVertex3f(3.75, 3, 0);
                glVertex3f(5.25, 3, 0);

                glVertex3f(3.75, 0, 0);
                glVertex3f(2.25, 0, 0);
                glVertex3f(2.25, 3, 0);
                glVertex3f(3.75, 3, 0);

                glVertex3f(2.25, 0, 0);
                glVertex3f(0.75, 0, 0);
                glVertex3f(0.75, 1, 0);
                glVertex3f(2.25, 1, 0);

                glVertex3f(2.25, 2.5, 0);
                glVertex3f(0.75, 2.5, 0);
                glVertex3f(0.75, 3, 0);
                glVertex3f(2.25, 3, 0);

                glVertex3f(0.75, 0, 0);
                glVertex3f(0, 0, 0);
                glVertex3f(0, 3, 0);
                glVertex3f(0.75, 3, 0);
            glEnd();

            // Murs maison (intérieurs)
            glBegin(GL_QUADS);
                glColor3f(0.4f, 0.4f, 0.4f);

                // Ch 1
                glVertex3f(3, 0, 0);
                glVertex3f(3, 0, 3);
                glVertex3f(3, 3, 3);
                glVertex3f(3, 3, 0);

                glVertex3f(3, 0, 3);
                glVertex3f(2.95, 0, 3);
                glVertex3f(2.95, 3, 3);
                glVertex3f(3, 3, 3);

                glVertex3f(2.95, 2, 3);
                glVertex3f(2.15, 2, 3);
                glVertex3f(2.15, 3, 3);
                glVertex3f(2.95, 3, 3);

                glVertex3f(2.15, 0, 3);
                glVertex3f(2, 0, 3);
                glVertex3f(2, 3, 3);
                glVertex3f(2.15, 3, 3);

                glVertex3f(2, 0, 3);
                glVertex3f(2, 0, 4);
                glVertex3f(2, 3, 4);
                glVertex3f(2, 3, 3);

                glVertex3f(0, 0, 4);
                glVertex3f(2.15, 0, 4);
                glVertex3f(2.15, 3, 4);
                glVertex3f(0, 3, 4);

                glVertex3f(2.15, 2, 4);
                glVertex3f(2.95, 2, 4);
                glVertex3f(2.95, 3, 4);
                glVertex3f(2.15, 3, 4);

                glVertex3f(2.95, 0, 4);
                glVertex3f(3, 0, 4);
                glVertex3f(3, 3, 4);
                glVertex3f(2.95, 3, 4);

                // Ch 2
                glVertex3f(6, 0, 0);
                glVertex3f(6, 0, 3);
                glVertex3f(6, 3, 3);
                glVertex3f(6, 3, 0);

                glVertex3f(3, 0, 3);
                glVertex3f(3.05, 0, 3);
                glVertex3f(3.05, 3, 3);
                glVertex3f(3, 3, 3);

                glVertex3f(3.05, 2, 3);
                glVertex3f(3.85, 2, 3);
                glVertex3f(3.85, 3, 3);
                glVertex3f(3.05, 3, 3);

                glVertex3f(3.85, 0, 3);
                glVertex3f(6, 0, 3);
                glVertex3f(6, 3, 3);
                glVertex3f(3.85, 3, 3);

                // Ch 3
                glVertex3f(9, 0, 0);
                glVertex3f(9, 0, 3);
                glVertex3f(9, 3, 3);
                glVertex3f(9, 3, 0);

                glVertex3f(6, 0, 3);
                glVertex3f(6.05, 0, 3);
                glVertex3f(6.05, 3, 3);
                glVertex3f(6, 3, 3);

                glVertex3f(6.05, 2, 3);
                glVertex3f(6.85, 2, 3);
                glVertex3f(6.85, 3, 3);
                glVertex3f(6.05, 3, 3);

                glVertex3f(6.85, 0, 3);
                glVertex3f(9, 0, 3);
                glVertex3f(9, 3, 3);
                glVertex3f(6.85, 3, 3);

                // Bains
                glVertex3f(3, 0, 8);
                glVertex3f(3, 0, 4);
                glVertex3f(3, 3, 4);
                glVertex3f(3, 3, 8);

                glVertex3f(5, 0, 8);
                glVertex3f(5, 0, 5);
                glVertex3f(5, 3, 5);
                glVertex3f(5, 3, 8);

                glVertex3f(3, 0, 4);
                glVertex3f(3.1, 0, 4);
                glVertex3f(3.1, 3, 4);
                glVertex3f(3, 3, 4);

                glVertex3f(3.1, 2, 4);
                glVertex3f(3.9, 2, 4);
                glVertex3f(3.9, 3, 4);
                glVertex3f(3.1, 3, 4);

                glVertex3f(3.9, 0, 4);
                glVertex3f(4, 0, 4);
                glVertex3f(4, 3, 4);
                glVertex3f(3.9, 3, 4);

                // Cuisine
                glVertex3f(8, 0, 8);
                glVertex3f(8, 0, 7.5);
                glVertex3f(8, 3, 7.5);
                glVertex3f(8, 3, 8);

                glVertex3f(8, 0, 5);
                glVertex3f(8, 0, 4);
                glVertex3f(8, 3, 4);
                glVertex3f(8, 3, 5);

                glVertex3f(8, 0, 5);
                glVertex3f(8, 0, 7.5);
                glVertex3f(8, 1.2, 7.5);
                glVertex3f(8, 1.2, 5);

                glVertex3f(8, 2.5, 5);
                glVertex3f(8, 2.5, 7.5);
                glVertex3f(8, 3, 7.5);
                glVertex3f(8, 3, 5);

                glVertex3f(6, 0, 4);
                glVertex3f(7.1, 0, 4);
                glVertex3f(7.1, 3, 4);
                glVertex3f(6, 3, 4);

                glVertex3f(7.1, 2, 4);
                glVertex3f(7.9, 2, 4);
                glVertex3f(7.9, 3, 4);
                glVertex3f(6, 3, 4);

                glVertex3f(7.9, 0, 4);
                glVertex3f(8, 0, 4);
                glVertex3f(8, 3, 4);
                glVertex3f(7.9, 3, 4);

                // Toilettes
                glVertex3f(4, 0, 4);
                glVertex3f(4, 0, 5);
                glVertex3f(4, 3, 5);
                glVertex3f(4, 3, 4);

                glVertex3f(4, 0, 5);
                glVertex3f(6, 0, 5);
                glVertex3f(6, 3, 5);
                glVertex3f(4, 3, 5);

                glVertex3f(6, 0, 4);
                glVertex3f(6, 0, 5);
                glVertex3f(6, 3, 5);
                glVertex3f(6, 3, 4);

                glVertex3f(4, 0, 4);
                glVertex3f(4.1, 0, 4);
                glVertex3f(4.1, 3, 4);
                glVertex3f(4, 3, 4);

                glVertex3f(4.1, 2, 4);
                glVertex3f(4.9, 2, 4);
                glVertex3f(4.9, 3, 4);
                glVertex3f(4.1, 3, 4);

                glVertex3f(4.9, 0, 4);
                glVertex3f(6, 0, 4);
                glVertex3f(6, 3, 4);
                glVertex3f(4.9, 3, 4);
            glEnd();

            // ------------ MEUBLES ------------

            // Cuisine :
            // Table
            glColor3f(0.4314f, 0.0431f, 0.0784f);
            glPushMatrix();
                glTranslatef(6.5f, 0.0f, 6.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.3f, 0.0f, 1.0f);
                    glVertex3f(0.3f, 0.9f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 1.0f);
                    glVertex3f(0.3f, 0.9f, 1.0f);
                    glVertex3f(0.3f, 0.9f, 0.0f);
                    
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    glVertex3f(1.5f, 0.9f, 1.0f);
                    glVertex3f(1.5f, 0.9f, 0.0f);
                    
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(1.5f, 0.9f, 0.0f);
                    glVertex3f(1.5f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 0.0f);
                    
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    glVertex3f(1.5f, 0.9f, 1.0f);
                    glVertex3f(1.5f, 1.0f, 1.0f);
                    glVertex3f(0.0f, 1.0f, 1.0f);
                    
                    glVertex3f(0.0f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 1.0f);
                    glVertex3f(1.5f, 1.0f, 1.0f);
                    glVertex3f(1.5f, 1.0f, 0.0f);
                glEnd();
            glPopMatrix();

            // Chaise
            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(6.9f, 0.0f, 5.8f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(7.5f, 0.0f, 5.8f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();
            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(7.25f, 0.0f, 7.2f);
                glRotatef(180, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();
            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(7.85f, 0.0f, 7.2f);
                glRotatef(180, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            // Plan de travail
            glPushMatrix();
                glColor3f(0.4314f, 0.0431f, 0.0784f);
                glTranslatef(5.0f, 0.0f, 5.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.5f, 0.0f, 0.8f);
                    glVertex3f(0.5f, 1.0f, 0.8f);
                    glVertex3f(0.5f, 1.0f, 3.0f);
                    glVertex3f(0.5f, 0.0f, 3.0f);

                    glVertex3f(0.5f, 1.0f, 0.8f);
                    glVertex3f(0.0f, 1.0f, 0.8f);
                    glVertex3f(0.0f, 1.0f, 3.0f);
                    glVertex3f(0.5f, 1.0f, 3.0f);

                    glVertex3f(0.3f, 1.8f, 0.8f);
                    glVertex3f(0.0f, 1.8f, 0.8f);
                    glVertex3f(0.0f, 1.8f, 3.0f);
                    glVertex3f(0.3f, 1.8f, 3.0f);

                    glVertex3f(0.3f, 1.8f, 0.8f);
                    glVertex3f(0.3f, 2.5f, 0.8f);
                    glVertex3f(0.3f, 2.5f, 3.0f);
                    glVertex3f(0.3f, 1.8f, 3.0f);

                    glVertex3f(0.3f, 2.5f, 0.8f);
                    glVertex3f(0.0f, 2.5f, 0.8f);
                    glVertex3f(0.0f, 2.5f, 3.0f);
                    glVertex3f(0.3f, 2.5f, 3.0f);

                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 2.5f, 0.0f);
                    glVertex3f(0.5f, 2.5f, 0.8f);
                    glVertex3f(0.5f, 0.0f, 0.8f);

                    glVertex3f(0.5f, 0.0f, 0.8f);
                    glVertex3f(0.0f, 0.0f, 0.8f);
                    glVertex3f(0.0f, 2.5f, 0.8f);
                    glVertex3f(0.5f, 2.5f, 0.8f);

                    glVertex3f(0.0f, 2.5f, 0.0f);
                    glVertex3f(0.5f, 2.5f, 0.0f);
                    glVertex3f(0.5f, 2.5f, 0.8f);
                    glVertex3f(0.0f, 2.5f, 0.8f);
                glEnd();
            glPopMatrix();

            // Bains :
            // Baignoire
            glPushMatrix();
                glTranslatef(3.0f, 0.0f, 7.0f);
                glBegin(GL_QUADS);
                    glColor3f(0.0353f, 0.3216f, 0.1569f);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(2.0f, 0.0f, 0.0f);
                    glVertex3f(2.0f, 0.7f, 0.0f);
                    glVertex3f(0.0f, 0.7f, 0.0f);

                    glColor3f(0.8f, 0.8f, 0.8f);
                    glVertex3f(0.1f, 0.1f, 0.1f);
                    glVertex3f(1.9f, 0.1f, 0.1f);
                    glVertex3f(1.9f, 0.1f, 1.0f);
                    glVertex3f(0.1f, 0.1f, 1.0f);

                    glColor3f(1.0f, 1.0f, 1.0f);
                    glVertex3f(0.0f, 0.0f, 0.1f);
                    glVertex3f(2.0f, 0.0f, 0.1f);
                    glVertex3f(2.0f, 0.7f, 0.1f);
                    glVertex3f(0.0f, 0.7f, 0.1f);

                    glVertex3f(0.1f, 0.1f, 0.1f);
                    glVertex3f(0.1f, 0.1f, 1.0f);
                    glVertex3f(0.1f, 0.7f, 1.0f);
                    glVertex3f(0.1f, 0.7f, 0.1f);

                    glVertex3f(1.9f, 0.1f, 0.1f);
                    glVertex3f(1.9f, 0.1f, 1.0f);
                    glVertex3f(1.9f, 0.7f, 1.0f);
                    glVertex3f(1.9f, 0.7f, 0.1f);

                    glVertex3f(0.0f, 0.7f, 0.0f);
                    glVertex3f(2.0f, 0.7f, 0.0f);
                    glVertex3f(2.0f, 0.7f, 0.1f);
                    glVertex3f(0.0f, 0.7f, 0.1f);

                    glVertex3f(0.0f, 0.7f, 0.1f);
                    glVertex3f(0.1f, 0.7f, 0.1f);
                    glVertex3f(0.1f, 0.7f, 1.0f);
                    glVertex3f(0.0f, 0.7f, 1.0f);

                    glVertex3f(1.9f, 0.7f, 0.1f);
                    glVertex3f(2.0f, 0.7f, 0.1f);
                    glVertex3f(2.0f, 0.7f, 1.0f);
                    glVertex3f(1.9f, 0.7f, 1.0f);

                    glVertex3f(0.1f, 0.1f, 0.999f);
                    glVertex3f(1.9f, 0.1f, 0.999f);
                    glVertex3f(1.9f, 0.7f, 0.999f);
                    glVertex3f(0.1f, 0.7f, 0.999f);
                glEnd();
            glPopMatrix();

            // Meuble
            glPushMatrix();
                glColor3f(0.8f, 0.8f, 0.8f);
                glTranslatef(4.5f, 0.3f, 5.5f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.5f, 0.0f, 1.0f);
                    glVertex3f(0.5f, 0.6f, 1.0f);
                    glVertex3f(0.0f, 0.6f, 1.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.6f, 1.0f);
                    glVertex3f(0.0f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.5f, 0.0f, 1.0f);
                    glVertex3f(0.5f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.6f, 1.0f);
                    glVertex3f(0.5f, 0.6f, 1.0f);
                    glVertex3f(0.5f, 0.6f, 0.0f);

                    glColor3f(1.0f, 1.0f, 1.0f);
                    glVertex3f(0.49f, 0.6f, 0.0f);
                    glVertex3f(0.49f, 0.6f, 1.0f);
                    glVertex3f(0.49f, 2.0f, 1.0f);
                    glVertex3f(0.49f, 2.0f, 0.0f);
                glEnd();
            glPopMatrix();

            // Tapis
            glPushMatrix();
                glColor3f(0.5216f, 0.5098f, 0.7059f);
                glTranslatef(3.2f, 0.0f, 6.2f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.01f, 0.0f);
                    glVertex3f(0.0f, 0.01f, 0.8f);
                    glVertex3f(1.3f, 0.01f, 0.8f);
                    glVertex3f(1.3f, 0.01f, 0.0f);
                glEnd();
            glPopMatrix();

            // Chambre 1
            // Lit
            glPushMatrix();
                glColor3f(0.2745f, 0.5451f, 0.6039f);
                glTranslatef(0.0f, 0.0f, 0.8f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.55f, 0.0f);
                    glVertex3f(2.0f, 0.55f, 0.0f);
                    glVertex3f(2.0f, 0.55f, 1.5f);
                    glVertex3f(0.0f, 0.55f, 1.5f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.55f, 0.0f);
                    glVertex3f(2.0f, 0.55f, 0.0f);
                    glVertex3f(2.0f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.5f);
                    glVertex3f(0.0f, 0.55f, 1.5f);
                    glVertex3f(2.0f, 0.55f, 1.5f);
                    glVertex3f(2.0f, 0.0f, 1.5f);

                    glVertex3f(2.0f, 0.0f, 0.0f);
                    glVertex3f(2.0f, 0.55f, 0.0f);
                    glVertex3f(2.0f, 0.55f, 1.5f);
                    glVertex3f(2.0f, 0.0f, 1.5f);

                    glColor3f(0.1961f, 0.102f, 0.0235f);
                    glVertex3f(0.01f, 0.55f, 0.0f);
                    glVertex3f(0.01f, 0.85f, 0.0f);
                    glVertex3f(0.01f, 0.85f, 1.5f);
                    glVertex3f(0.01f, 0.55f, 1.5f);
                glEnd();
            glPopMatrix();
            
            // Coussin
            glPushMatrix();
                glColor3f(0.7529f, 0.6118f, 0.0f);
                glTranslatef(0.2f, 0.55f, 0.95f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.0f);
                    glVertex3f(0.0f, 0.1f, 0.0f);

                    glVertex3f(0.0f, 0.0, 0.5f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.0f);

                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.0f);

                    glVertex3f(0.0f, 0.1f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.7529f, 0.6118f, 0.0f);
                glTranslatef(0.2f, 0.55f, 1.65f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.0f);
                    glVertex3f(0.0f, 0.1f, 0.0f);

                    glVertex3f(0.0f, 0.0, 0.5f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.0f);

                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.5f, 0.1f, 0.0f);

                    glVertex3f(0.0f, 0.1f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.0f);
                    glVertex3f(0.5f, 0.1f, 0.5f);
                    glVertex3f(0.0f, 0.1f, 0.5f);
                glEnd();
            glPopMatrix();

            // Table de chevet
            glPushMatrix();
                glColor3f(0.3451f, 0.1608f, 0.0f);
                glTranslatef(0.0f, 0.4f, 0.3f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.0f, 0.0f, 0.4f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.0f, 0.05f, 0.4f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.0f, 0.05f, 0.4f);

                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.3451f, 0.1608f, 0.0f);
                glTranslatef(0.0f, 0.4f, 2.4f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.0f, 0.0f, 0.4f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.0f, 0.05f, 0.4f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.0f, 0.05f, 0.4f);

                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.4f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                glEnd();
            glPopMatrix();

            // Placard
            glPushMatrix();
                glColor3f(0.1961f, 0.102f, 0.0235f);
                glTranslatef(0.75f, 0.0f, 3.25f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(1.25f, 0.0f, 0.0f);
                    glVertex3f(1.25f, 3.0f, 0.0f);
                    glVertex3f(0.0f, 3.0f, 0.0f);
                glEnd();
            glPopMatrix();

            // Etagère
            glPushMatrix();
                glColor3f(0.1961f, 0.102f, 0.0235f);
                glTranslatef(2.7f, 0.7f, 1.05f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.3f, 0.0f, 1.0f);
                    glVertex3f(0.3f, 0.05f, 1.0f);
                    glVertex3f(0.0f, 0.05f, 1.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 1.0f);
                    glVertex3f(0.0f, 0.05f, 1.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.05f, 1.0f);
                    glVertex3f(0.0f, 0.05f, 0.0f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.1961f, 0.102f, 0.0235f);
                glTranslatef(2.7f, 0.95f, 1.2f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.7f);
                    glVertex3f(0.3f, 0.0f, 0.7f);
                    glVertex3f(0.3f, 0.05f, 0.7f);
                    glVertex3f(0.0f, 0.05f, 0.7f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.7f);
                    glVertex3f(0.0f, 0.0f, 0.7f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.0f);
                    glVertex3f(0.3f, 0.05f, 0.7f);
                    glVertex3f(0.0f, 0.05f, 0.7f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 0.7f);
                    glVertex3f(0.0f, 0.05f, 0.7f);
                    glVertex3f(0.0f, 0.05f, 0.0f);
                glEnd();
            glPopMatrix();

            // TV
            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(2.99f, 1.2f, 1.05f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.56f, 1.0f);
                    glVertex3f(0.0f, 0.56f, 0.0f);
                glEnd();
            glPopMatrix();

            // Chambre 2 :
            // Lit
            glPushMatrix();
                glTranslatef(5.0f, 0.0f, 0.0f);
                glPushMatrix();
                    glColor3f(0.3451f, 0.1608f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 1.8f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.3f, 0.0f);
                        glVertex3f(0.0f, 0.3f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.9f, 0.0f, 1.8f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.3f, 0.0f);
                        glVertex3f(0.0f, 0.3f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.9f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glColor3f(0.0863f, 0.4588f, 0.3216f);
                    glTranslatef(0.0f, 0.3f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 0.0f);

                        glVertex3f(0.0f, 0.2f, 0.0f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glColor3f(1.0f, 1.0f, 1.0f);
                    glTranslatef(0.1f, 0.5f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.0f);

                        glVertex3f(0.8f, 0.0f, 0.0f);
                        glVertex3f(0.8f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.0f);

                        glVertex3f(0.0f, 0.1f, 0.0f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                    glEnd();
                glPopMatrix();
            glPopMatrix();

            // Bureau
            glPushMatrix();
                glTranslatef(3.1f, 0.0f, 0.1f);
                glRotatef(-90, 1.0f, 0.0f, 0.0f);
                glColor3f(1.0f, 1.0f, 1.0f);
                GLUquadricObj *pied1;
                pied1 = gluNewQuadric();
                gluQuadricNormals(pied1, GLU_SMOOTH);
                gluCylinder(pied1, 0.025, 0.025, 0.9, 32, 32);
            glPopMatrix();

            glPushMatrix();
                glTranslatef(3.6f, 0.0f, 0.1f);
                glRotatef(-90, 1.0f, 0.0f, 0.0f);
                glColor3f(1.0f, 1.0f, 1.0f);
                GLUquadricObj *pied2;
                pied2 = gluNewQuadric();
                gluQuadricNormals(pied2, GLU_SMOOTH);
                gluCylinder(pied2, 0.025, 0.025, 0.9, 32, 32);
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.749f, 0.5216f, 0.302f);
                glTranslatef(3.0f, 0.9f, 0.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 0.0f);

                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.0f, 0.05f, 1.8f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(1.0f, 1.0f, 1.0f);
                glTranslatef(3.0f, 0.0f, 1.4f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.4f);
                    glVertex3f(0.0f, 0.9f, 0.4f);

                    glVertex3f(0.75f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.0f);
                glEnd();
            glPopMatrix();

            // Chaise
            glPushMatrix();
                // Pied
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(4.0f, 0.0f, 0.3f);
                glRotatef(-90, 0.0f, 1.0f, 0.0f);
                
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();


            // Chambre 3 :
            // Lit
            glPushMatrix();
                glTranslatef(8.0f, 0.0f, 0.0f);
                glPushMatrix();
                    glColor3f(0.3451f, 0.1608f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 1.8f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.3f, 0.0f);
                        glVertex3f(0.0f, 0.3f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.9f, 0.0f, 1.8f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.0f, 0.0f);
                        glVertex3f(0.1f, 0.3f, 0.0f);
                        glVertex3f(0.0f, 0.3f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.9f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.0f, 0.1f);
                        glVertex3f(0.1f, 0.3f, 0.1f);
                        glVertex3f(0.0f, 0.3f, 0.1f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glColor3f(0.8745f, 0.4275f, 0.0784f);
                    glTranslatef(0.0f, 0.3f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 0.0f);

                        glVertex3f(0.0f, 0.2f, 0.0f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.0f, 1.9f);
                        glVertex3f(1.0f, 0.2f, 1.9f);
                        glVertex3f(0.0f, 0.2f, 1.9f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glColor3f(1.0f, 1.0f, 1.0f);
                    glTranslatef(0.1f, 0.5f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.0f);

                        glVertex3f(0.8f, 0.0f, 0.0f);
                        glVertex3f(0.8f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.0f);

                        glVertex3f(0.0f, 0.1f, 0.0f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.0f, 0.4f);
                        glVertex3f(0.8f, 0.1f, 0.4f);
                        glVertex3f(0.0f, 0.1f, 0.4f);
                    glEnd();
                glPopMatrix();
            glPopMatrix();

            // Bureau
            glPushMatrix();
                glTranslatef(6.1f, 0.0f, 0.1f);
                glRotatef(-90, 1.0f, 0.0f, 0.0f);
                glColor3f(0.7098f, 0.0588f, 0.0588f);
                GLUquadricObj *pied3;
                pied3 = gluNewQuadric();
                gluQuadricNormals(pied3, GLU_SMOOTH);
                gluCylinder(pied3, 0.025, 0.025, 0.9, 32, 32);
            glPopMatrix();

            glPushMatrix();
                glTranslatef(6.6f, 0.0f, 0.1f);
                glRotatef(-90, 1.0f, 0.0f, 0.0f);
                GLUquadricObj *pied4;
                pied4 = gluNewQuadric();
                gluQuadricNormals(pied4, GLU_SMOOTH);
                gluCylinder(pied4, 0.025, 0.025, 0.9, 32, 32);
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.749f, 0.5216f, 0.302f);
                glTranslatef(6.0f, 0.9f, 0.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.05f, 0.0f);
                    glVertex3f(0.0f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 0.0f);

                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.0f, 1.8f);
                    glVertex3f(0.8f, 0.05f, 1.8f);
                    glVertex3f(0.0f, 0.05f, 1.8f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.7098f, 0.0588f, 0.0588f);
                glTranslatef(6.0f, 0.0f, 1.4f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.4f);
                    glVertex3f(0.0f, 0.9f, 0.4f);

                    glVertex3f(0.75f, 0.0f, 0.0f);
                    glVertex3f(0.75f, 0.0f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.4f);
                    glVertex3f(0.75f, 0.9f, 0.0f);
                glEnd();
            glPopMatrix();

            // Chaise
            glPushMatrix();
                // Pieds
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(7.0f, 0.0f, 0.3f);
                glRotatef(-90, 0.0f, 1.0f, 0.0f);
                
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            // Toilettes
            glPushMatrix();
                glTranslatef(5.25f, 0.0f, 4.25f);
                glColor3f(1.0f, 1.0f, 1.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.0f);
                    glVertex3f(0.0f, 0.5f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.5f, 0.5f);
                    glVertex3f(0.0f, 0.5f, 0.5f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 0.5f);
                    glVertex3f(0.0f, 0.5f, 0.5f);
                    glVertex3f(0.0f, 0.5f, 0.0f);

                    glVertex3f(0.5f, 0.0f, 0.0f);
                    glVertex3f(0.5f, 0.0f, 0.5f);
                    glVertex3f(0.5f, 0.5f, 0.5f);
                    glVertex3f(0.5f, 0.5f, 0.0f);

                    glVertex3f(0.1f, 0.0f, 0.1f);
                    glVertex3f(0.4f, 0.0f, 0.1f);
                    glVertex3f(0.4f, 0.5f, 0.1f);
                    glVertex3f(0.1f, 0.5f, 0.1f);

                    glVertex3f(0.1f, 0.0f, 0.4f);
                    glVertex3f(0.4f, 0.0f, 0.4f);
                    glVertex3f(0.4f, 0.5f, 0.4f);
                    glVertex3f(0.1f, 0.5f, 0.4f);

                    glVertex3f(0.1f, 0.0f, 0.1f);
                    glVertex3f(0.1f, 0.0f, 0.4f);
                    glVertex3f(0.1f, 0.5f, 0.4f);
                    glVertex3f(0.1f, 0.5f, 0.1f);

                    glVertex3f(0.4f, 0.0f, 0.1f);
                    glVertex3f(0.4f, 0.0f, 0.4f);
                    glVertex3f(0.4f, 0.5f, 0.4f);
                    glVertex3f(0.4f, 0.5f, 0.1f);

                    glColor3f(0.9216f, 0.7843f, 0.698f);

                    glVertex3f(0.0f, 0.5f, 0.0f);
                    glVertex3f(0.1f, 0.5f, 0.0f);
                    glVertex3f(0.1f, 0.5f, 0.5f);
                    glVertex3f(0.0f, 0.5f, 0.5f);

                    glVertex3f(0.4f, 0.5f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.5f);
                    glVertex3f(0.4f, 0.5f, 0.5f);

                    glVertex3f(0.0f, 0.5f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.1f);
                    glVertex3f(0.0f, 0.5f, 0.1f);

                    glVertex3f(0.0f, 0.5f, 0.4f);
                    glVertex3f(0.5f, 0.5f, 0.4f);
                    glVertex3f(0.5f, 0.5f, 0.5f);
                    glVertex3f(0.0f, 0.5f, 0.5f);

                    glVertex3f(0.5f, 0.5f, 0.0f);
                    glVertex3f(0.5f, 0.5f, 0.5f);
                    glVertex3f(0.5f, 1.0f, 0.5f);
                    glVertex3f(0.5f, 1.0f, 0.0f);

                    glVertex3f(-0.1f, 0.6f, 0.67f);
                    glVertex3f(-0.2f, 0.6f, 0.67f);
                    glVertex3f(-0.2f, 0.6f, 0.75f);
                    glVertex3f(-0.1f, 0.6f, 0.75f);

                    glVertex3f(-0.1f, 0.65f, 0.67f);
                    glVertex3f(-0.2f, 0.65f, 0.67f);
                    glVertex3f(-0.2f, 0.65f, 0.75f);
                    glVertex3f(-0.1f, 0.65f, 0.75f);

                    glVertex3f(-0.1f, 0.60f, 0.67f);
                    glVertex3f(-0.1f, 0.65f, 0.67f);
                    glVertex3f(-0.1f, 0.65f, 0.75f);
                    glVertex3f(-0.1f, 0.60f, 0.75f);

                    glVertex3f(-0.2f, 0.60f, 0.67f);
                    glVertex3f(-0.2f, 0.65f, 0.67f);
                    glVertex3f(-0.2f, 0.65f, 0.75f);
                    glVertex3f(-0.2f, 0.60f, 0.75f);
                glEnd();
                glBegin(GL_TRIANGLES);
                    glColor3f(0.6f, 0.6f, 0.6f);
                    glVertex3f(0.1f, 0.5f, 0.1f);
                    glVertex3f(0.4f, 0.5f, 0.1f);
                    glVertex3f(0.25f, 0.1f, 0.25f);

                    glColor3f(0.7f, 0.7f, 0.7f);
                    glVertex3f(0.1f, 0.5f, 0.1f);
                    glVertex3f(0.1f, 0.5f, 0.4f);
                    glVertex3f(0.25f, 0.1f, 0.25f);

                    glColor3f(0.6f, 0.6f, 0.6f);
                    glVertex3f(0.4f, 0.5f, 0.4f);
                    glVertex3f(0.1f, 0.5f, 0.4f);
                    glVertex3f(0.25f, 0.1f, 0.25f);

                    glColor3f(0.7f, 0.7f, 0.7f);
                    glVertex3f(0.4f, 0.5f, 0.4f);
                    glVertex3f(0.4f, 0.5f, 0.1f);
                    glVertex3f(0.25f, 0.1f, 0.25f);
                glEnd();
            glPopMatrix();

            // Canapé
            glPushMatrix();
                glTranslatef(8.0f, 0.0f, 5.25f);
                glColor3f(0.0f, 0.0f, 0.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(1.0f, 0.0f, 0.0f);
                    glVertex3f(1.0f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 2.0f);
                    glVertex3f(1.0f, 0.0f, 2.0f);
                    glVertex3f(1.0f, 0.4f, 2.0f);
                    glVertex3f(0.0f, 0.4f, 2.0f);

                    glVertex3f(1.0f, 0.0f, 0.0f);
                    glVertex3f(1.0f, 0.0f, 2.0f);
                    glVertex3f(1.0f, 0.4f, 2.0f);
                    glVertex3f(1.0f, 0.4f, 0.0f);

                    glColor3f(0.05f, 0.05f, 0.05f);
                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(1.0f, 0.4f, 0.0f);
                    glVertex3f(1.0f, 0.4f, 2.0f);
                    glVertex3f(0.0f, 0.4f, 2.0f);
                    glColor3f(0.0f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(0.2f, 0.4f, 0.0f);
                    glVertex3f(0.2f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);

                    glVertex3f(0.0f, 0.4f, 2.0f);
                    glVertex3f(0.2f, 0.4f, 2.0f);
                    glVertex3f(0.2f, 0.9f, 2.0f);
                    glVertex3f(0.0f, 0.9f, 2.0f);

                    glVertex3f(0.2f, 0.4f, 0.0f);
                    glVertex3f(0.2f, 0.4f, 2.0f);
                    glVertex3f(0.2f, 0.9f, 2.0f);
                    glVertex3f(0.2f, 0.9f, 0.0f);

                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.2f, 0.9f, 0.0f);
                    glVertex3f(0.2f, 0.9f, 2.0f);
                    glVertex3f(0.0f, 0.9f, 2.0f);

                    glVertex3f(0.2f, 0.4f, 0.0f);
                    glVertex3f(1.0f, 0.4f, 0.0f);
                    glVertex3f(1.0f, 0.5f, 0.0f);
                    glVertex3f(0.2f, 0.5f, 0.0f);

                    glVertex3f(0.2f, 0.4f, 0.3f);
                    glVertex3f(1.0f, 0.4f, 0.3f);
                    glVertex3f(1.0f, 0.5f, 0.3f);
                    glVertex3f(0.2f, 0.5f, 0.3f);

                    glVertex3f(1.0f, 0.4f, 0.0f);
                    glVertex3f(1.0f, 0.4f, 0.3f);
                    glVertex3f(1.0f, 0.5f, 0.3f);
                    glVertex3f(1.0f, 0.5f, 0.0f);

                    glVertex3f(0.2f, 0.5f, 0.0f);
                    glVertex3f(1.0f, 0.5f, 0.0f);
                    glVertex3f(1.0f, 0.5f, 0.3f);
                    glVertex3f(0.2f, 0.5f, 0.3f);

                    glVertex3f(0.2f, 0.4f, 1.7f);
                    glVertex3f(1.0f, 0.4f, 1.7f);
                    glVertex3f(1.0f, 0.5f, 1.7f);
                    glVertex3f(0.2f, 0.5f, 1.7f);

                    glVertex3f(0.2f, 0.4f, 2.0f);
                    glVertex3f(1.0f, 0.4f, 2.0f);
                    glVertex3f(1.0f, 0.5f, 2.0f);
                    glVertex3f(0.2f, 0.5f, 2.0f);

                    glVertex3f(1.0f, 0.4f, 1.7f);
                    glVertex3f(1.0f, 0.4f, 2.0f);
                    glVertex3f(1.0f, 0.5f, 2.0f);
                    glVertex3f(1.0f, 0.5f, 1.7f);

                    glVertex3f(0.2f, 0.5f, 1.7f);
                    glVertex3f(1.0f, 0.5f, 1.7f);
                    glVertex3f(1.0f, 0.5f, 2.0f);
                    glVertex3f(0.2f, 0.5f, 2.0f);
                glEnd();
            glPopMatrix();

            // Petite table
            glPushMatrix();
                glTranslatef(10.0f, 0.0f, 5.75f);
                glColor3f(0.0f, 0.0f, 0.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.8f, 0.0f, 1.0f);
                    glVertex3f(0.8f, 0.4f, 1.0f);
                    glVertex3f(0.0f, 0.4f, 1.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.4f, 1.0f);
                    glVertex3f(0.0f, 0.4f, 0.0f);

                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 1.0f);
                    glVertex3f(0.8f, 0.4f, 1.0f);
                    glVertex3f(0.8f, 0.4f, 0.0f);

                    glColor3f(0.8706f, 0.7216f, 0.5294f);
                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 1.0f);
                    glVertex3f(0.8f, 0.4f, 1.0f);
                    glVertex3f(0.8f, 0.4f, 0.0f);
                glEnd();
            glPopMatrix();

            // Fauteuil
            glPushMatrix();
                glTranslatef(10.0f, 0.0f, 4.2f);
                glColor3f(0.0f, 0.0f, 0.0f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.8f);
                    glVertex3f(0.8f, 0.0f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.8f);
                    glVertex3f(0.0f, 0.4f, 0.8f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 0.8f);
                    glVertex3f(0.0f, 0.4f, 0.8f);
                    glVertex3f(0.0f, 0.4f, 0.0f);

                    glVertex3f(0.8f, 0.0f, 0.0f);
                    glVertex3f(0.8f, 0.0f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.0f);

                    glColor3f(0.05f, 0.05f, 0.05f);
                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.0f);
                    glColor3f(0.0f, 0.0f, 0.0f);

                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(0.0f, 0.4f, 0.2f);
                    glVertex3f(0.0f, 0.8f, 0.2f);
                    glVertex3f(0.0f, 0.8f, 0.0f);

                    glVertex3f(0.8f, 0.4f, 0.0f);
                    glVertex3f(0.8f, 0.4f, 0.2f);
                    glVertex3f(0.8f, 0.8f, 0.2f);
                    glVertex3f(0.8f, 0.8f, 0.0f);

                    glVertex3f(0.0f, 0.4f, 0.0f);
                    glVertex3f(0.8f, 0.4f, 0.0f);
                    glVertex3f(0.8f, 0.8f, 0.0f);
                    glVertex3f(0.0f, 0.8f, 0.0f);

                    glVertex3f(0.0f, 0.4f, 0.2f);
                    glVertex3f(0.8f, 0.4f, 0.2f);
                    glVertex3f(0.8f, 0.8f, 0.2f);
                    glVertex3f(0.0f, 0.8f, 0.2f);

                    glVertex3f(0.0f, 0.8f, 0.0f);
                    glVertex3f(0.8f, 0.8f, 0.0f);
                    glVertex3f(0.8f, 0.8f, 0.2f);
                    glVertex3f(0.0f, 0.8f, 0.2f);

                    glVertex3f(0.0f, 0.4f, 0.8f);
                    glVertex3f(0.15f, 0.4f, 0.8f);
                    glVertex3f(0.15f, 0.6f, 0.8f);
                    glVertex3f(0.0f, 0.6f, 0.8f);

                    glVertex3f(0.0f, 0.4f, 0.2f);
                    glVertex3f(0.0f, 0.4f, 0.8f);
                    glVertex3f(0.0f, 0.6f, 0.8f);
                    glVertex3f(0.0f, 0.6f, 0.2f);

                    glVertex3f(0.15f, 0.4f, 0.2f);
                    glVertex3f(0.15f, 0.4f, 0.8f);
                    glVertex3f(0.15f, 0.6f, 0.8f);
                    glVertex3f(0.15f, 0.6f, 0.2f);

                    glVertex3f(0.0f, 0.6f, 0.2f);
                    glVertex3f(0.15f, 0.6f, 0.2f);
                    glVertex3f(0.15f, 0.6f, 0.8f);
                    glVertex3f(0.0f, 0.6f, 0.8f);

                    glVertex3f(0.65f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.6f, 0.8f);
                    glVertex3f(0.65f, 0.6f, 0.8f);

                    glVertex3f(0.65f, 0.4f, 0.2f);
                    glVertex3f(0.65f, 0.4f, 0.8f);
                    glVertex3f(0.65f, 0.6f, 0.8f);
                    glVertex3f(0.65f, 0.6f, 0.2f);

                    glVertex3f(0.8f, 0.4f, 0.2f);
                    glVertex3f(0.8f, 0.4f, 0.8f);
                    glVertex3f(0.8f, 0.6f, 0.8f);
                    glVertex3f(0.8f, 0.6f, 0.2f);

                    glVertex3f(0.65f, 0.6f, 0.2f);
                    glVertex3f(0.8f, 0.6f, 0.2f);
                    glVertex3f(0.8f, 0.6f, 0.8f);
                    glVertex3f(0.65f, 0.6f, 0.8f);
                glEnd();
            glPopMatrix();
            
            // Télé salon
            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(12.9f, 1.0f, 5.25f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 2.0f);
                    glVertex3f(0.0f, 1.1f, 2.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                glEnd();
            glPopMatrix();

            // Table salon
            glPushMatrix();
                glTranslatef(10.6f, 0.0f, 1.1f);
                glColor3f(0.0f, 0.0f, 0.0f);
                glPushMatrix();
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.75f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 1.35f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                    glEnd();
                glPopMatrix();

                glPushMatrix();
                    glTranslatef(0.75f, 0.0f, 1.35f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.05f);
                        glVertex3f(0.0f, 0.9f, 0.0f);

                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.05f);
                        glVertex3f(0.05f, 0.9f, 0.0f);
                    glEnd();
                glPopMatrix();

                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.8f, 0.9f, 0.0f);
                    glVertex3f(0.8f, 0.9f, 1.4f);
                    glVertex3f(0.0f, 0.9f, 1.4f);

                    glVertex3f(0.0f, 1.0f, 0.0f);
                    glVertex3f(0.8f, 1.0f, 0.0f);
                    glVertex3f(0.8f, 1.0f, 1.4f);
                    glVertex3f(0.0f, 1.0f, 1.4f);

                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.8f, 0.9f, 0.0f);
                    glVertex3f(0.8f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 0.0f);

                    glVertex3f(0.0f, 0.9f, 1.4f);
                    glVertex3f(0.8f, 0.9f, 1.4f);
                    glVertex3f(0.8f, 1.0f, 1.4f);
                    glVertex3f(0.0f, 1.0f, 1.4f);

                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 1.4f);
                    glVertex3f(0.0f, 1.0f, 1.4f);
                    glVertex3f(0.0f, 1.0f, 0.0f);

                    glVertex3f(0.8f, 0.9f, 0.0f);
                    glVertex3f(0.8f, 0.9f, 1.4f);
                    glVertex3f(0.8f, 1.0f, 1.4f);
                    glVertex3f(0.8f, 1.0f, 0.0f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(10.3f, 0.0f, 2.35f);
                glRotatef(90, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(11.7f, 0.0f, 1.25f);
                glRotatef(-90, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(11.7f, 0.0f, 2.0f);
                glRotatef(-90, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            glPushMatrix();
                glColor3f(0.0f, 0.0f, 0.0f);
                glTranslatef(10.3f, 0.0f, 1.6f);
                glRotatef(90, 0.0f, 1.0f, 0.0f);
                // Pied de chaise
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.0f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.3f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                        glVertex3f(0.0f, 0.6f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.05f);
                        glVertex3f(0.0f, 0.6f, 0.0f);
                        
                        glVertex3f(0.05f, 0.0f, 0.0f);
                        glVertex3f(0.05f, 0.0f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.05f);
                        glVertex3f(0.05f, 0.6f, 0.0f);
                    glEnd();
                glPopMatrix();
                glBegin(GL_QUADS);
                    // Assise
                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);

                    glVertex3f(0.0f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.35f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.0f);
                    glVertex3f(0.35f, 0.6f, 0.35f);
                    glVertex3f(0.0f, 0.6f, 0.35f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.35f);
                    glVertex3f(0.0f, 0.65f, 0.35f);
                    
                    // Dossier
                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 0.65f, 0.0f);

                    glVertex3f(0.0f, 0.65f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 0.65f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 0.65f, 0.05f);

                    glVertex3f(0.35f, 0.65f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.35f, 0.65f, 0.05f);

                    glVertex3f(0.0f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.0f);
                    glVertex3f(0.35f, 1.1f, 0.05f);
                    glVertex3f(0.0f, 1.1f, 0.05f);
                glEnd();
            glPopMatrix();

            // Bibliothèque
            glPushMatrix();
                glTranslatef(9.0f, 0.0f, 0.45f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 1.7f, 0.0f);
                    glVertex3f(0.0f, 1.7f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 0.05f);
                    glVertex3f(0.3f, 0.0f, 0.05f);
                    glVertex3f(0.3f, 1.7f, 0.05f);
                    glVertex3f(0.0f, 1.7f, 0.05f);

                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 0.05f);
                    glVertex3f(0.3f, 1.7f, 0.05f);
                    glVertex3f(0.3f, 1.7f, 0.0f);

                    glVertex3f(0.0f, 1.7f, 0.0f);
                    glVertex3f(0.3f, 1.7f, 0.0f);
                    glVertex3f(0.3f, 1.7f, 0.9f);
                    glVertex3f(0.0f, 1.7f, 0.9f);

                    glVertex3f(0.0f, 1.65f, 0.05f);
                    glVertex3f(0.3f, 1.65f, 0.05f);
                    glVertex3f(0.3f, 1.65f, 0.85f);
                    glVertex3f(0.0f, 1.65f, 0.85f);

                    glVertex3f(0.0f, 1.7f, 0.9f);
                    glVertex3f(0.3f, 1.7f, 0.9f);
                    glVertex3f(0.3f, 0.9f, 0.9f);
                    glVertex3f(0.0f, 0.9f, 0.9f);

                    glVertex3f(0.0f, 1.65f, 0.85f);
                    glVertex3f(0.3f, 1.65f, 0.85f);
                    glVertex3f(0.3f, 0.85f, 0.85f);
                    glVertex3f(0.0f, 0.85f, 0.85f);

                    glVertex3f(0.3f, 0.9f, 0.85f);
                    glVertex3f(0.3f, 0.9f, 0.9f);
                    glVertex3f(0.3f, 1.7f, 0.9f);
                    glVertex3f(0.3f, 1.7f, 0.85f);

                    glVertex3f(0.3f, 0.0f, 0.0f);
                    glVertex3f(0.3f, 0.0f, 2.1f);
                    glVertex3f(0.3f, 0.05f, 2.1f);
                    glVertex3f(0.3f, 0.05f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 2.1f);
                    glVertex3f(0.3f, 0.0f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 2.1f);
                    glVertex3f(0.0f, 2.1f, 2.1f);

                    glVertex3f(0.3f, 0.05f, 2.05f);
                    glVertex3f(0.3f, 0.05f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 2.05f);

                    glVertex3f(0.0f, 0.0f, 2.05f);
                    glVertex3f(0.3f, 0.0f, 2.05f);
                    glVertex3f(0.3f, 2.1f, 2.05f);
                    glVertex3f(0.0f, 2.1f, 2.05f);

                    glVertex3f(0.0f, 2.1f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 1.6f);
                    glVertex3f(0.0f, 2.1f, 1.6f);

                    glVertex3f(0.0f, 0.9f, 1.6f);
                    glVertex3f(0.3f, 0.9f, 1.6f);
                    glVertex3f(0.3f, 2.1f, 1.6f);
                    glVertex3f(0.0f, 2.1f, 1.6f);

                    glVertex3f(0.0f, 0.85f, 1.65f);
                    glVertex3f(0.3f, 0.85f, 1.65f);
                    glVertex3f(0.3f, 2.1f, 1.65f);
                    glVertex3f(0.0f, 2.1f, 1.65f);

                    glVertex3f(0.3f, 0.9f, 1.6f);
                    glVertex3f(0.3f, 0.9f, 1.65f);
                    glVertex3f(0.3f, 2.05f, 1.65f);
                    glVertex3f(0.3f, 2.05f, 1.6f);

                    glVertex3f(0.3f, 2.05f, 1.6f);
                    glVertex3f(0.3f, 2.05f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 2.1f);
                    glVertex3f(0.3f, 2.1f, 1.6f);

                    glVertex3f(0.3f, 1.65f, 0.05f);
                    glVertex3f(0.3f, 1.65f, 0.85f);
                    glVertex3f(0.3f, 1.7f, 0.85f);
                    glVertex3f(0.3f, 1.7f, 0.05f);

                    glVertex3f(0.3f, 0.9f, 0.9f);
                    glVertex3f(0.3f, 0.9f, 0.9f);
                    glVertex3f(0.3f, 2.05f, 1.6f);
                    glVertex3f(0.3f, 2.05f, 1.6f);

                    glVertex3f(0.0f, 0.9f, 0.9f);
                    glVertex3f(0.3f, 0.9f, 0.9f);
                    glVertex3f(0.3f, 0.9f, 1.6f);
                    glVertex3f(0.0f, 0.9f, 1.6f);

                    glVertex3f(0.0f, 0.85f, 0.85f);
                    glVertex3f(0.3f, 0.85f, 0.85f);
                    glVertex3f(0.3f, 0.85f, 1.65f);
                    glVertex3f(0.0f, 0.85f, 1.65f);

                    glVertex3f(0.3f, 0.85f, 0.85f);
                    glVertex3f(0.3f, 0.85f, 1.65f);
                    glVertex3f(0.3f, 0.9f, 1.65f);
                    glVertex3f(0.3f, 0.9f, 0.85f);

                    glVertex3f(0.0f, 0.05f, 0.05f);
                    glVertex3f(0.3f, 0.05f, 0.05f);
                    glVertex3f(0.3f, 0.05f, 2.05f);
                    glVertex3f(0.0f, 0.05f, 2.05f);

                    glVertex3f(0.0f, 0.05f, 0.45f);
                    glVertex3f(0.3f, 0.05f, 0.45f);
                    glVertex3f(0.3f, 1.65f, 0.45f);
                    glVertex3f(0.0f, 1.65f, 0.45f);

                    glVertex3f(0.0f, 0.05f, 0.85f);
                    glVertex3f(0.3f, 0.05f, 0.85f);
                    glVertex3f(0.3f, 0.85f, 0.85f);
                    glVertex3f(0.0f, 0.85f, 0.85f);

                    glVertex3f(0.0f, 0.05f, 1.25f);
                    glVertex3f(0.3f, 0.05f, 1.25f);
                    glVertex3f(0.3f, 0.85f, 1.25f);
                    glVertex3f(0.0f, 0.85f, 1.25f);

                    glVertex3f(0.0f, 0.05f, 1.65f);
                    glVertex3f(0.3f, 0.05f, 1.65f);
                    glVertex3f(0.3f, 0.85f, 1.65f);
                    glVertex3f(0.0f, 0.85f, 1.65f);

                    glVertex3f(0.0f, 0.45f, 0.05f);
                    glVertex3f(0.3f, 0.45f, 0.05f);
                    glVertex3f(0.3f, 0.45f, 2.05f);
                    glVertex3f(0.0f, 0.45f, 2.05f);

                    glVertex3f(0.0f, 0.85f, 0.05f);
                    glVertex3f(0.3f, 0.85f, 0.05f);
                    glVertex3f(0.3f, 0.85f, 0.85f);
                    glVertex3f(0.0f, 0.85f, 0.85f);

                    glVertex3f(0.0f, 1.25f, 0.05f);
                    glVertex3f(0.3f, 1.25f, 0.05f);
                    glVertex3f(0.3f, 1.25f, 0.85f);
                    glVertex3f(0.0f, 1.25f, 0.85f);

                    glVertex3f(0.0f, 0.85f, 1.65f);
                    glVertex3f(0.3f, 0.85f, 1.65f);
                    glVertex3f(0.3f, 0.85f, 2.05f);
                    glVertex3f(0.0f, 0.85f, 2.05f);

                    glVertex3f(0.0f, 1.25f, 1.65f);
                    glVertex3f(0.3f, 1.25f, 1.65f);
                    glVertex3f(0.3f, 1.25f, 2.05f);
                    glVertex3f(0.0f, 1.25f, 2.05f);

                    glVertex3f(0.0f, 1.65f, 1.65f);
                    glVertex3f(0.3f, 1.65f, 1.65f);
                    glVertex3f(0.3f, 1.65f, 2.05f);
                    glVertex3f(0.0f, 1.65f, 2.05f);
                glEnd();
            glPopMatrix();

            // Meuble vitré
            glPushMatrix();
                glTranslatef(12.6f, 0.0f, 3.5f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.4f, 0.0f, 0.0f);
                    glVertex3f(0.4f, 2.0f, 0.0f);
                    glVertex3f(0.0f, 2.0f, 0.0f);

                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.4f, 0.0f, 1.0f);
                    glVertex3f(0.4f, 2.0f, 1.0f);
                    glVertex3f(0.0f, 2.0f, 1.0f);

                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 0.0f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    glVertex3f(0.0f, 0.9f, 0.0f);

                    glVertex3f(0.0f, 0.9f, 0.0f);
                    glVertex3f(0.0f, 0.9f, 0.1f);
                    glVertex3f(0.0f, 2.0f, 0.1f);
                    glVertex3f(0.0f, 2.0f, 0.0f);

                    glVertex3f(0.0f, 0.9f, 0.9f);
                    glVertex3f(0.0f, 0.9f, 1.0f);
                    glVertex3f(0.0f, 2.0f, 1.0f);
                    glVertex3f(0.0f, 2.0f, 0.9f);

                    glVertex3f(0.0f, 1.9f, 0.0f);
                    glVertex3f(0.0f, 1.9f, 1.0f);
                    glVertex3f(0.0f, 2.0f, 1.0f);
                    glVertex3f(0.0f, 2.0f, 0.0f);

                    glVertex3f(0.0f, 2.0f, 0.0f);
                    glVertex3f(0.0f, 2.0f, 1.0f);
                    glVertex3f(0.4f, 2.0f, 1.0f);
                    glVertex3f(0.4f, 2.0f, 0.0f);

                    glColor3f(0.2f, 0.2f, 0.2f);
                    glVertex3f(0.39f, 0.0f, 0.0f);
                    glVertex3f(0.39f, 0.0f, 1.0f);
                    glVertex3f(0.39f, 2.0f, 1.0f);
                    glVertex3f(0.39f, 2.0f, 0.0f);
                glEnd();
            glPopMatrix();

            if(toit==true){
                // Plafond maison
                glBegin(GL_QUADS);
                    glColor3f(0.2f, 0.2f, 0.2f);
                    glVertex3f(0, 3, 0);
                    glVertex3f(0, 3, 8);
                    glVertex3f(13, 3, 8);
                    glVertex3f(13, 3, 0);
                glEnd();

                // Grand comble gauche
                glBegin(GL_TRIANGLES);
                    glColor3f(0.8f, 0.8f, 0.8f);
                    glVertex3f(0, 3, 0);
                    glVertex3f(0, 4, 4);
                    glVertex3f(0, 3, 8);
                glEnd();

                // Grand comble droit
                glBegin(GL_TRIANGLES);
                    glVertex3f(13, 3, 0);
                    glVertex3f(13, 4, 4);
                    glVertex3f(13, 3, 8);
                glEnd();

                // Petit comble gauche
                glBegin(GL_TRIANGLES);
                    glVertex3f(0, 2.25, 8);
                    glVertex3f(0, 2.25, 11);
                    glVertex3f(0, 3, 8);
                glEnd();

                // Petit comble droit
                glBegin(GL_TRIANGLES);
                    glVertex3f(3, 2.25, 8);
                    glVertex3f(3, 2.25, 11);
                    glVertex3f(3, 3, 8);
                glEnd();

                // Toit arrière
                glBegin(GL_QUADS);
                    glColor3f(0.5f, 0.2f, 0.1f);
                    glVertex3f(0, 3, 0);
                    glVertex3f(0, 4, 4);
                    glVertex3f(13, 4, 4);
                    glVertex3f(13, 3, 0);
                glEnd();

                // Toit avant
                glBegin(GL_QUADS);
                    glVertex3f(0, 4, 4);
                    glVertex3f(0, 3, 8);
                    glVertex3f(13, 3, 8);
                    glVertex3f(13, 4, 4);
                glEnd();

                // Petit toit
                glBegin(GL_QUADS);
                    glVertex3f(0, 3, 8);
                    glVertex3f(0, 2.25, 11);
                    glVertex3f(3, 2.25, 11);
                    glVertex3f(3, 3, 8);
                glEnd();
            }    

            // Verre / eau
            glDepthMask( GL_FALSE );
            glBegin(GL_QUADS);
                glColor4f(0.8f, 0.8f, 0.8f, 0.5f);

                glVertex3f(5.75, 0, 8);
                glVertex3f(7.25, 0, 8);
                glVertex3f(7.25, 2, 8);
                glVertex3f(5.75, 2, 8);

                glVertex3f(10.5, 0, 8);
                glVertex3f(12, 0, 8);
                glVertex3f(12, 2, 8);
                glVertex3f(10.5, 2, 8);

                glVertex3f(13, 0, 2.5);
                glVertex3f(13, 0, 1);
                glVertex3f(13, 2, 1);
                glVertex3f(13, 2, 2.5);

                glVertex3f(8.25, 1, 0);
                glVertex3f(6.75, 1, 0);
                glVertex3f(6.75, 2.5, 0);
                glVertex3f(8.25, 2.5, 0);

                glVertex3f(5.25, 1, 0);
                glVertex3f(3.75, 1, 0);
                glVertex3f(3.75, 2.5, 0);
                glVertex3f(5.25, 2.5, 0);

                glVertex3f(2.25, 1, 0);
                glVertex3f(0.75, 1, 0);
                glVertex3f(0.75, 2.5, 0);
                glVertex3f(2.25, 2.5, 0);
            glEnd();

            // Verrière cuisine
            glPushMatrix();
                glBegin(GL_QUADS);
                    glVertex3f(8, 1.2, 5);
                    glVertex3f(8, 1.2, 7.5);
                    glVertex3f(8, 2.5, 7.5);
                    glVertex3f(8, 2.5, 5);
                glEnd();
            glPopMatrix();

            // Eau baignoire
            glPushMatrix();
                glTranslatef(3.1f, 0.6f, 7.1f);
                glColor4f(0.0f, 1.0f, 1.0f, 0.5f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(1.8f, 0.0f, 0.0f);
                    glVertex3f(1.8f, 0.0f, 0.9f);
                    glVertex3f(0.0f, 0.0f, 0.9f);
                glEnd();
            glPopMatrix();

            // Vitre meuble salon
            glPushMatrix();
                glTranslatef(12.6f, 0.9f, 3.6f);
                glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
                glBegin(GL_QUADS);
                    glVertex3f(0.0f, 0.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 0.0f);
                    glVertex3f(0.0f, 1.0f, 0.8f);
                    glVertex3f(0.0f, 0.0f, 0.8f);
                glEnd();
            glPopMatrix();

            glDepthMask( GL_TRUE );
        glPopMatrix();

    glPopMatrix();
}