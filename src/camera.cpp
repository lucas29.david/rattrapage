#include "../include/camera.h"
#include "../include/autoCam.h"
#include <cstdlib>

int width;
int height;
bool freecam = false;
float theta = 0;
float phi = M_PI;
float r = 5;
float posCamX = 0;
float posCamY = 0;
float posCamZ = 0;
float pViseX = posCamX+r*cos(theta)*sin(phi);
float pViseY = posCamY+r*sin(theta);
float pViseZ = posCamZ+r*cos(theta)*cos(phi);
float leftX = sin(phi+M_PI/2);
float leftY = 0;
float leftZ = cos(phi+M_PI/2);
float upX = sin(theta)*leftZ-cos(theta)*cos(phi)*leftY;
float upY = cos(theta)*cos(phi)*leftX-cos(theta)*sin(phi)*leftZ;
float upZ = cos(theta)*sin(phi)*leftY-sin(theta)*leftX;
bool toit = true;
bool stop = false;
bool pause = false;

void newPos(int count){
  if (count==0){
    posCamX=p[count].posCamX;
    posCamY=p[count].posCamY;
    posCamZ=p[count].posCamZ;
    pViseX=p[count].pViseX;
    pViseY=p[count].pViseY;
    pViseZ=p[count].pViseZ;
    upX=p[count].upX;
    upY=p[count].upY;
    upZ=p[count].upZ;
    glutPostRedisplay();
  }
  else if(stop==true){return;}
  else if(pause==false){
    if(abs(posCamX-p[count].posCamX)>=0.001){
      posCamX+=0.01*(p[count].posCamX-p[count-1].posCamX);
      posCamY+=0.01*(p[count].posCamY-p[count-1].posCamY);
      posCamZ+=0.01*(p[count].posCamZ-p[count-1].posCamZ);
      pViseX+=0.01*(p[count].pViseX-p[count-1].pViseX);
      pViseY+=0.01*(p[count].pViseY-p[count-1].pViseY);
      pViseZ+=0.01*(p[count].pViseZ-p[count-1].pViseZ);
      upX+=0.01*(p[count].upX-p[count-1].upX);
      upY+=0.01*(p[count].upY-p[count-1].upY);
      upZ+=0.01*(p[count].upZ-p[count-1].upZ);

      glutPostRedisplay();
      glutTimerFunc(1000/60, newPos, count);
    }
    else if(count<13){
      count++;
      newPos(count);
    }
  }
  else{
    glutTimerFunc(1000/60, newPos, count);
  }
}

void keyboard(unsigned char key, int x, int y){
  if(key==27){
    exit(0);
  }
  if (key=='f'){
    if(freecam==true){
      freecam=false;
      pause=false;
      newPos(0);
    }
    else{
      stop=true;
      freecam=true;
    }
  }
  if (freecam==true){  
    switch (key)
    {
      case 'z':
        posCamX+=0.5*cos(theta)*sin(phi);
        posCamY+=0.5*sin(theta);
        posCamZ+=0.5*cos(theta)*cos(phi);
        break;
      case 's':
        posCamX-=0.5*cos(theta)*sin(phi);
        posCamY-=0.5*sin(theta);
        posCamZ-=0.5*cos(theta)*cos(phi);
        break;
      case 'q':
        posCamX+=0.5*leftX;
        posCamZ+=0.5*leftZ;
        break;
      case 'd':
        posCamX-=0.5*leftX;
        posCamZ-=0.5*leftZ;
        break;

      case 't':
        if(toit==true){
          toit=false;
        }
        else{
          toit=true;
        }
        break;
    }
    glutTimerFunc(1000/60, update, 0);
  }
  else if(key==32){
    if(posCamX==p[0].posCamX){
      if(stop==true){
        stop=false;
      }
      newPos(1);
    }
    else{
      stop=true;
      if(pause==true){
        pause=false;
      }
      newPos(0);
    }
  }
  else if(key=='p'){
    if(pause==false && posCamX!=p[0].posCamX){
      pause=true;
    }
    else if(pause==true){
      pause=false;
    }
  }
}

void speKeyboard(int key, int x, int y){
  if (freecam==true){ 
    switch(key){
      case GLUT_KEY_UP:
        theta+=M_PI/16;
      break;
      case GLUT_KEY_DOWN:
        theta-=M_PI/16;
      break;
      case GLUT_KEY_LEFT:
        phi+=M_PI/16;
      break;
      case GLUT_KEY_RIGHT:
        phi-=M_PI/16;
      break;
      case 112:
        posCamY+=0.5;
      break;
      case 114:
        posCamY-=0.5;
      break;
    }
    glutTimerFunc(1000/60, update, 0);
  }
}

// Fonctionnel mais pas encore optimisé
// void mouse(int x, int y){
  // if (freecam==true){
  //   if (x<=width*0.2){
  //     phi+=M_PI/1024;
  //   }
  //   if (x>=width*0.8){
  //     phi-=M_PI/1024;
  //   }
  //   if (y<=height*0.2){
  //     theta+=M_PI/1024;
  //   }
  //   if (y>=height*0.8){
  //     theta-=M_PI/1024;
  //   }
  //   glutTimerFunc(1000/60, update, 0);
  //   // int centerX = (float)width/2.0;
  //   // int centerY = (float)height/2.0;
  //   // glutWarpPointer(centerX, centerY); pour recentrer la souris au milieu de l'écran mais ça crash car la fenetre n'est pas encore initialisée
  // }
// }

void update(int i){
  pViseX = posCamX+r*cos(theta)*sin(phi);
  pViseY = posCamY+r*sin(theta);
  pViseZ = posCamZ+r*cos(theta)*cos(phi);
  leftX = sin(phi+M_PI/2);
  leftY = 0;
  leftZ = cos(phi+M_PI/2);
  upX = sin(theta)*leftZ-cos(theta)*cos(phi)*leftY;
  upY = cos(theta)*cos(phi)*leftX-cos(theta)*sin(phi)*leftZ;
  upZ = cos(theta)*sin(phi)*leftY-sin(theta)*leftX;
  // cout << "posCamX : " << posCamX << " || posCamY : " << posCamY << " || posCamZ : " << posCamZ << endl;
  // cout << "pViseX : " << pViseX << " || pViseY : " << pViseY << " || pViseZ : " << pViseZ << endl;
  // cout << "upX : " << upX << " || upY : " << upY << " || upZ : " << upZ << endl;
  glutPostRedisplay();
}