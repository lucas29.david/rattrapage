#include "autoCam.h"

Position p[];

Position positions(){
    p[0].posCamX=4.357;
    p[0].posCamY=6;
    p[0].posCamZ=10.6333;
    p[0].pViseX=2.58923;
    p[0].pViseY=4.08658;
    p[0].pViseZ=6.36557;
    p[0].upX=-0.146447;
    p[0].upY=0.92388;
    p[0].upZ=-0.353553;

    p[1].posCamX=1.56182;
    p[1].posCamY=1.5;
    p[1].posCamZ=2.0435;
    p[1].pViseX=5.71917;
    p[1].pViseY=1.01677;
    p[1].pViseZ=-0.73435;
    p[1].upX=0;
    p[1].upY=1;
    p[1].upZ=0;

    p[2].posCamX=6.0;
    p[2].posCamY=1.5;
    p[2].posCamZ=-1.72924;
    p[2].pViseX=1.51228;
    p[2].pViseY=1.5;
    p[2].pViseZ=-1.72924;
    p[2].upX=0;
    p[2].upY=1;
    p[2].upZ=0;

    p[3].posCamX=1.5;
    p[3].posCamY=1.5;
    p[3].posCamZ=-1.97437;
    p[3].pViseX=-1.95449;
    p[3].pViseY=1;
    p[3].pViseZ=-1.97438;
    p[3].upX=-0.19509;
    p[3].upY=0.980785;
    p[3].upZ=0;

    p[4].posCamX=1.25618;
    p[4].posCamY=1.5;
    p[4].posCamZ=-1.17594;
    p[4].pViseX=-1.52167;
    p[4].pViseY=1.11431;
    p[4].pViseZ=2.98141;
    p[4].upX=0;
    p[4].upY=1;
    p[4].upZ=0;

    p[5].posCamX=-0.300559;
    p[5].posCamY=1.5;
    p[5].posCamZ=-2.75179;
    p[5].pViseX=3.16704;
    p[5].pViseY=0.550925;
    p[5].pViseZ=-6.21939;
    p[5].upX=0.13795;
    p[5].upY=0.980785;
    p[5].upZ=-0.13795;

    p[6].posCamX=-0.0313506;
    p[6].posCamY=1.5;
    p[6].posCamZ=-1.97438;
    p[6].pViseX=-5.03135;
    p[6].pViseY=0.93721;
    p[6].pViseZ=-1.97439;
    p[6].upX=0;
    p[6].upY=1;
    p[6].upZ=0;

    // toilettes
    p[7].posCamX=-2.39062;
    p[7].posCamY=1.5;
    p[7].posCamZ=-1.99554;
    p[7].pViseX=0.333857;
    p[7].pViseY=0.0649995;
    p[7].pViseZ=2.08193;
    p[7].upX=0.108387;
    p[7].upY=0.980785;
    p[7].upZ=0.162212;

    p[8].posCamX=-3.42697;
    p[8].posCamY=1.5;
    p[8].posCamZ=-2.37;
    p[8].pViseX=-0.7025;
    p[8].pViseY=0.521243;
    p[8].pViseZ=-6.44747;
    p[8].upX=0.108386;
    p[8].upY=0.980785;
    p[8].upZ=-0.162212;

    p[9].posCamX=-3;
    p[9].posCamY=1.5;
    p[9].posCamZ=-0.959641;
    p[9].pViseX=-0.6167;
    p[9].pViseY=1.20406;
    p[9].pViseZ=3.19771;
    p[9].upX=0;
    p[9].upY=1;
    p[9].upZ=0;

    p[10].posCamX=-3.8;
    p[10].posCamY=1.5;
    p[10].posCamZ=-2.8;
    p[10].pViseX=-6.33157;
    p[10].pViseY=0.94185;
    p[10].pViseZ=-6.52854;
    p[10].upX=-0.108386;
    p[10].upY=0.980785;
    p[10].upZ=-0.162212;

    p[11].posCamX=-3.72106;
    p[11].posCamY=1.5;
    p[11].posCamZ=-1.73018;
    p[11].pViseX=-6.44552;
    p[11].pViseY=0.6213;
    p[11].pViseZ=2.3473;
    p[11].upX=-0.108386;
    p[11].upY=0.980785;
    p[11].upZ=0.162212;

    p[12].posCamX=-4.67743;
    p[12].posCamY=1.5;
    p[12].posCamZ=4.56109;
    p[12].pViseX=-5.65288;
    p[12].pViseY=0.966113;
    p[12].pViseZ=9.46502;
    p[12].upX=0;
    p[12].upY=1;
    p[12].upZ=0;

    p[13].posCamX=-5.63055;
    p[13].posCamY=5.94195;
    p[13].posCamZ=13.7533;
    p[13].pViseX=-3.86278;
    p[13].pViseY=4.02853;
    p[13].pViseZ=9.48551;
    p[13].upX=0.146447;
    p[13].upY=0.92388;
    p[13].upZ=-0.353553;

    return *p;
}