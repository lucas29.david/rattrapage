#define GL_GLEXT_PROTOTYPES
// #include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <iostream>
#include "../include/autoCam.h"
#include "../include/camera.h"
#include "../include/draw.h"

using namespace std;

void init(){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  glClearColor(0.6,0.6,0.6,0.0);  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(70.0f, (float)width/height, 0.1f, 100.0f);  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void display(void) {
  init();
  gluLookAt(posCamX, posCamY, posCamZ, pViseX, pViseY, pViseZ, upX, upY, upZ);

  draw(toit);
  glutSwapBuffers(); 
  }

int main(int argc, char **argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    width = glutGet(GLUT_SCREEN_WIDTH);
    height = glutGet(GLUT_SCREEN_HEIGHT);
    glutInitWindowSize(width,height);  
    glutInitWindowPosition(0, 0);  
    glutCreateWindow("Fenetre OpenGL");
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL); 
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glutSetCursor(GLUT_CURSOR_NONE);

    *p=positions();
    posCamX=p[0].posCamX;
    posCamY=p[0].posCamY;
    posCamZ=p[0].posCamZ;
    pViseX=p[0].pViseX;
    pViseY=p[0].pViseY;
    pViseZ=p[0].pViseZ;
    upX=p[0].upX;
    upY=p[0].upY;
    upZ=p[0].upZ;

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard); 
    glutSpecialFunc(speKeyboard); 
    // Contrôle à la souris fonctionnel mais à optimiser (capturer la souris dans la fenêtre et déplacer le point de vue en fonction des déplacements relatifs de la souris sur l'écran)
    // glutPassiveMotionFunc(mouse);
    glutMainLoop() ;  
    return(0);
}