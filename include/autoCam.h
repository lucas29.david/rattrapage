#ifndef AUTOCAM_H
#define AUTOCAM_H

struct Position {
    float posCamX;
    float posCamY;
    float posCamZ;
    float pViseX;
    float pViseY;
    float pViseZ;
    float upX;
    float upY;
    float upZ;
};

extern Position p[14];

Position positions();

#endif