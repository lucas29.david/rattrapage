#ifndef CAMERA_H
#define CAMERA_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <cmath>
#include <iostream>
using namespace std;

extern int width;
extern int height;
extern bool freecam;
extern float theta;
extern float phi;
extern float r;
extern float posCamX;
extern float posCamY;
extern float posCamZ;
extern float pViseX;
extern float pViseY;
extern float pViseZ;
extern float upX;
extern float upY;
extern float upZ;
extern bool stop;
extern bool pause;

void newPos(int count);
void keyboard(unsigned char key, int x, int y);
void speKeyboard(int key, int x, int y);
void mouse(int x, int y);
void update(int i);

#endif